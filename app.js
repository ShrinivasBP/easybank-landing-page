window.addEventListener("load", () => {
  let button = true;
  const navPopup = document.getElementById("nav-popup");
  const navIcon = document.getElementById("nav-icon");
  const navBar = document.getElementById("nav-bar");
  navBar.style.visibility = "hidden";
  navPopup.addEventListener("click", () => {
    if (button) {
      button = false;
      navBar.style.visibility = "visible";
      // navBar.style.display= "flex"
      // navBar.style.flexDirection="column"
      navIcon.src = "images/icon-close.svg";
    } else {
      button = true;
      navBar.style.visibility = "hidden";
      navIcon.src = "images/icon-hamburger.svg";
    }
  });
});
